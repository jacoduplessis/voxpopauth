from allauth.socialaccount.providers.oauth2.urls import default_urlpatterns

from .provider import VoxPopProvider

urlpatterns = default_urlpatterns(VoxPopProvider)
