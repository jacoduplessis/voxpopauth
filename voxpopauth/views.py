import requests

from allauth.socialaccount.providers.oauth2.views import (
    OAuth2Adapter,
    OAuth2CallbackView,
    OAuth2LoginView,
)

from .provider import VoxPopProvider


class VoxPopOAuth2Adapter(OAuth2Adapter):
    provider_id = VoxPopProvider.id

    access_token_url = 'http://id.voxpop.co.za/oauth/token/'
    authorize_url = 'http://id.voxpop.co.za/oauth/authorize/'
    profile_url = 'http://id.voxpop.co.za/api/me/'

    def complete_login(self, request, app, token, **kwargs):
        resp = requests.get(self.profile_url, params={'access_token': token.token})
        extra_data = resp.json()
        provider = self.get_provider()
        login = provider.sociallogin_from_response(request, extra_data)
        return login


oauth2_login = OAuth2LoginView.adapter_view(VoxPopOAuth2Adapter)
oauth2_callback = OAuth2CallbackView.adapter_view(VoxPopOAuth2Adapter)