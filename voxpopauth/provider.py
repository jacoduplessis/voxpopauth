from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider


class VoxPopAccount(ProviderAccount):

    def get_profile_url(self):
        return None

    def get_avatar_url(self):
        return None

    def to_str(self):
        dflt = super().to_str()
        return self.account.extra_data.get('username', dflt)


class VoxPopProvider(OAuth2Provider):
    id = 'voxpop'
    name = 'VoxPop'
    account_class = VoxPopAccount


    def get_default_scope(self):
        return ['read']

    def extract_uid(self, data):
        return str(data['id'])

    def extract_common_fields(self, data):
        return {
            'username': data.get('username'),
            'email': data.get('email'),
        }


provider_classes = [VoxPopProvider]