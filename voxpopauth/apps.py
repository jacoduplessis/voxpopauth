from django.apps import AppConfig


class VoxpopauthConfig(AppConfig):
    name = 'voxpopauth'
